export const MENU_COMMANDS = {
  SHOW_UPLOAD_UI: 'show_upload_ui',
  RESET: 'reset',
};

export const CLIENT_STORAGE_KEYS = {
  GITLAB_USER_CONFIG: 'GITLAB_ACCESS_TOKEN', // GITLAB_ACCESS_TOKEN must be maintained for legacy reasons
};
